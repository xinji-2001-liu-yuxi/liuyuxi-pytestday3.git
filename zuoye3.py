import pytest
import requests
import allure


@allure.feature("宠物商店宠物信息接口测试")
class TestPetstore:
    def setup_class(self):
        # 基础的数据信息
        pass

    @allure.story("查询宠物接口冒烟用例")
    def test_getpet(self):
        '''
        获取宠物信息
        :return:
        '''
        self.url = 'https://petstore.swagger.io/v2/pet/2'
        with allure.step("发出查询接口请求"):
            r = requests.get(self.url)
        with allure.step("获取查询接口响应"):
            print(r.json())
        with allure.step("查询接口断言"):
            assert r.status_code == 404
